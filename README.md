# VMJ: Variability Modules for Java

## Introduction

We attempt to implement delta-oriented software product line (SPL) with Java programming language using VMJ.
The idea is start from solving challenge case about interoperability of SPL. The init repository is [here](https://gitlab.com/mayaretno/splc-challenge-mspl/tree/java9). The case study is about a railway system that include Signal, Switch, and Station. 

In this repository, we use case study Adaptive Information System for Charity Organization (AISCO).

The features in AISCO are:
1. Program: Regular Activity (_mandatory_)
2. Program: Operational
3. Financial Report: Income (_mandatory_)
4. Financial Report: Expense
5. Financial Report: Frequency
6. Donation: via AISCO
7. Donation: via Payment Gateway (call another product line)

There are three products:
1. Charity School. Features: Activity, Operational, Income, Expense, PaymentGateway.
2. Yayasan Pandhu. Features: Activity, Income, Frequency.
3. Hilfuns. Features: Activity, Income, Expense, PaymentGateway.


## Modeling SPL with Java
- Product line is created as a Java Project. 
- Features in feature diagram are created as a Java module. Create packages for implementing the feature.
- Delta modules are created inside the package, use the decorator pattern. 
- Product is defined as a Java module. Create packages inside the module. 
The implementation is used to simulate the running process when the product is generated. 
- Product generation is done with build script [genproduct.sh](genproduct.sh).
If product validation is success, the product is built successfully. 
Choose your product, build the artifacts, and run the product. 

### Structure Directory. 
Modules inside `src` folder:
- aisco.financialreport.core
- aisco.financialreport.income
- aisco.financialreport.expense
- aisco.financialreport.frequency
- aisco.program.activity
- aisco.program.operational
- aisco.donation.core
- aisco.donation.paymentgateway
- aisco.product.charityschool (Product: CharitySchool)
- aisco.product.yayasanpandhu (Product: YayasanPandhu)
- aisco.product.hilfuns (Product: Hilfuns)

Notes: external modules are available inside folder `external`.

### How to Build and Run (Product Generation)
Build: `bash genproduct.sh [ProductNameModule] [ProductName]`

Run: `java --module-path [ProductNameModule] -m [ProductNameModule]`


List of ProductNameModule and Product Name (Main Class)
```
Module aisco.product.charityschool | CharitySchool
Module aisco.product.yayasanpandhu | YayasanPandhu
Module aisco.product.hilfuns | Hilfe
```

#### Example CharitySchool:
Build: `bash genproduct.sh aisco.product.charityschool CharitySchool`

Run: `java --module-path aisco.product.charityschool -m aisco.product.charityschool`


The examples can also be run inside a Docker container:

```bash
docker build --tag aisco/java-example:latest .
docker run --rm -it aisco/java-example:latest

# Now inside the interactive terminal

## To build'Charity School' product example:
bash genproduct.sh aisco.product.charityschool CharitySchool

## To run 'Charity School' product example:
java --module-path aisco.product.charityschool -m aisco.product.charityschool