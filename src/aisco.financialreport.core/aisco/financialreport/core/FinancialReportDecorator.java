package aisco.financialreport.core;

import aisco.program.activity.Program;
import java.util.ArrayList;
import java.util.List;

public abstract class FinancialReportDecorator extends FinancialReportComponent {
    public FinancialReportComponent record;

    public FinancialReportDecorator (FinancialReportComponent record) {
        this.record = record;
    }

    public int getAmount()
    {
        return record.getAmount();
    }

    public String getDescription()
    {
        return record.getDescription();
    }

    public Program getProgram ()
    {

        return record.getProgram();
    }


    public abstract void printHeader();

    public String toString() {
        return "- " + record.getDescription() + ": " + record.getAmount() + " for Program" + record.getProgram() + ".\n";
    }

        /* delta adds method */
    public int totalAmount(List<FinancialReport> records) {
        int sum = 0;
        for(int i = 0; i < records.size(); i++){
            int o = ((FinancialReport)records.get(i)).getAmount();
            sum = sum+o;
            }
        return sum;
    }
}
