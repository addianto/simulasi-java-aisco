module aisco.product.yayasanpandhu {
    requires aisco.program.activity;
    requires aisco.financialreport.core;
    requires aisco.financialreport.income;
    requires aisco.financialreport.frequency;
}