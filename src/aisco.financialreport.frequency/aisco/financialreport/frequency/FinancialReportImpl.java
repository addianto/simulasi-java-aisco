package aisco.financialreport.frequency;

import aisco.financialreport.core.FinancialReport;
import aisco.financialreport.core.FinancialReportDecorator;
import aisco.financialreport.core.FinancialReportComponent;


/** implementation feature */
public class FinancialReportImpl extends FinancialReportComponent{
    DFrequency abc;
    
    public FinancialReportImpl(FinancialReportComponent record, String frequency) {
        abc = new DFrequency (record, frequency);
    }

    public String toString() {
        return abc.toString();
    }

    public int getAmount()
    {
        return abc.getAmount();
    }
 
    public void printHeader()
    {
        //throw new UnsupportedOperationException();
        abc.printHeader();
    }
    
}


