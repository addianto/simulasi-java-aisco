package aisco.financialreport.frequency.reports;

import aisco.financialreport.core.FinancialReport;
import aisco.financialreport.core.FinancialReportDecorator;
import aisco.financialreport.core.FinancialReportComponent;
import aisco.financialreport.frequency.DFrequency;


/** implementation feature IncomeWithFrequency */
public class FinancialReportImpl extends FinancialReportComponent{
    DFrequency abc;
    
    public FinancialReportImpl(FinancialReportDecorator record, String frequency) {
        abc = new DFrequency (record, frequency);
    }

    public String toString() {
        return abc.toString();
    }
    
    public int getAmount()
    {
        return abc.getAmount();
    }

    
    public void printHeader()
    {
        //throw new UnsupportedOperationException();
        abc.printHeader();
    }
    
}


