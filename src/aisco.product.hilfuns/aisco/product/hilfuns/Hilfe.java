package aisco.product.hilfuns;
import aisco.program.activity.Program;
import aisco.program.ProgramFactory;
import aisco.financialreport.core.FinancialReport;
import aisco.financialreport.FinancialReportFactory;
import aisco.financialreport.income.*;
import aisco.financialreport.expense.*;
import aisco.donation.core.Donation;
import aisco.donation.DonationFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Hilfe {
    public static void main(String[] args) {
        System.out.println("Product Hilfuns");
        //Create instance of program
        Program disasterrelief= ProgramFactory.createProgram("aisco.program.activity.ProgramImpl", 1, "Flood in Bogor", "Helping flood victims", "100 families", "Fast Furious", "https://www.hilfe.splelive.id/logohilfe");
        Program freemedicine = ProgramFactory.createProgram("aisco.program.activity.ProgramImpl", 2, "Free Medicine", "Free treatment and medicine for poor people", "50 persons", "Healthy Clinic", "https://www.hilfe.splelive.id/logopengobatan");

        //Create instance of Incomes
        List<FinancialReport> incomes = new ArrayList<>();
        FinancialReport income1 = FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl",FinancialReportFactory.createFinancialReport("aisco.financialreport.core.FinancialReportImpl","1", "23-11-2019", 1000000, "Donation Nadia", disasterrelief, "42010"), "Transfer");
        incomes.add(income1);
        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", FinancialReportFactory.createFinancialReport("aisco.financialreport.core.FinancialReportImpl","2", "25-11-2019", 800000, "Donation Lentera", freemedicine, "42010"), "Cash"));
        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl",FinancialReportFactory.createFinancialReport("aisco.financialreport.core.FinancialReportImpl","3", "29-11-2019", 5000000, "Fundraising Mitra University", disasterrelief, "42000"), "Transfer"));
        income1.printHeader();
        System.out.println(incomes);
        ((aisco.financialreport.income.FinancialReportImpl)income1).sumIncome(incomes);

        //Create instance of Expenses
        List<FinancialReport> expenses = new ArrayList<>();
        FinancialReport expense1 =FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl",FinancialReportFactory.createFinancialReport("aisco.financialreport.core.FinancialReportImpl","10", "29-11-2019", 1000000, "Rice", disasterrelief, "50200"));
        expenses.add(expense1);
        expenses.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl", FinancialReportFactory.createFinancialReport("aisco.financialreport.core.FinancialReportImpl","20", "30-11-2019", 350000, "Paracetamol", freemedicine, "410")));
        expenses.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl", FinancialReportFactory.createFinancialReport("aisco.financialreport.core.FinancialReportImpl","30", "30-11-2019", 3500000, "Mattress and Blanket", disasterrelief, "50200")));
        expense1.printHeader();
        System.out.println(expenses);
        ((aisco.financialreport.expense.FinancialReportImpl) expense1).sumExpense(expenses);

        Donation donate = DonationFactory.createDonation("aisco.donation.pgateway.DonationImpl");
        donate.addDonation();
        donate.getDonation();
    }
}
