module aisco.product.hilfuns {
    requires aisco.program.activity;
    requires aisco.financialreport.core;
    requires aisco.financialreport.income;
    requires aisco.financialreport.expense;
    requires aisco.donation.core;
    requires aisco.donation.pgateway;
}