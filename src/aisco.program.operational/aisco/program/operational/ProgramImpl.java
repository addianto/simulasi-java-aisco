package aisco.program.operational;
import aisco.program.activity.ProgramComponent;

public class ProgramImpl extends ProgramDecorator {

    // public ProgramImpl(ProgramComponent program)
    // {
    //     super(program);
    // }

    /** delta removes attributes, modifies the constructor */
    public ProgramImpl(Integer idProgram, String name, String description, String target)
    {
        super(idProgram, name, description, target);
    }

    public String toString(){
        return " Operational " + name + "";
    }

}
